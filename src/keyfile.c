 /*
  *  keyfile.c
  *
  *  This file is part of gncpmount.
  *
  *  gncpmount is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  *
  *  gncpmount is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License
  *  along with gncpmount.  If not, see <http://www.gnu.org/licenses/>.
  *
  *  Author: Jan Lipovsky <janlipovsky@gmail.com>
  *
  */
#include "keyfile.h"
#include "dialogs.h"


/**
*   Load cmd_params from gkeyfile
*/
void load_cmdparams_from_file (const gchar *filename)
{
    GKeyFile *keyfile;
    GError *error = NULL;

    keyfile = g_key_file_new ();

    if (!g_key_file_load_from_file (keyfile, filename, G_KEY_FILE_KEEP_COMMENTS | G_KEY_FILE_KEEP_TRANSLATIONS, &error))
    {
        g_warning(error->message);
        show_message( GTK_MESSAGE_ERROR,"ERROR: %s", "Key file load from file failed","%s",error->message);
        g_error_free(error);
        return;
    }


    cmd_params.username = g_key_file_get_string (keyfile, "ncpmount_options", "username", NULL);

/*
    cmd_params.password = g_key_file_get_string (keyfile, "ncpmount_options", "password", NULL);
*/

    cmd_params.server = g_key_file_get_string (keyfile, "ncpmount_options", "server", NULL);
    cmd_params.dns_name = g_key_file_get_string (keyfile, "ncpmount_options", "dns_name", NULL);
    cmd_params.volume = g_key_file_get_string (keyfile, "ncpmount_options", "volume", NULL);
    cmd_params.uid = g_key_file_get_string (keyfile, "ncpmount_options", "uid", NULL);
    cmd_params.gid = g_key_file_get_string (keyfile, "ncpmount_options", "gid", NULL);
    cmd_params.fmode = g_key_file_get_string (keyfile, "ncpmount_options", "fmode", NULL);
    cmd_params.dmode = g_key_file_get_string (keyfile, "ncpmount_options", "dmode", NULL);
    cmd_params.time_out = g_key_file_get_string (keyfile, "ncpmount_options", "time_out", NULL);
    cmd_params.retry_count = g_key_file_get_string (keyfile, "ncpmount_options", "retry_count", NULL);
    cmd_params.level = g_key_file_get_string (keyfile, "ncpmount_options", "level", NULL);
    cmd_params.charset = g_key_file_get_string (keyfile, "ncpmount_options", "charset", NULL);
    cmd_params.codepage = g_key_file_get_string (keyfile, "ncpmount_options", "codepage", NULL);
    cmd_params.extraoptions = g_key_file_get_string (keyfile, "ncpmount_options", "extraoptions", NULL);
    cmd_params.mount_point = g_key_file_get_string (keyfile, "ncpmount_options", "mount_point", NULL);


    cmd_params.C = g_key_file_get_boolean (keyfile, "ncpmount_options", "C", NULL);
    cmd_params.n = g_key_file_get_boolean (keyfile, "ncpmount_options", "n", NULL);
    cmd_params.s = g_key_file_get_boolean (keyfile, "ncpmount_options", "s", NULL);
    cmd_params.b = g_key_file_get_boolean (keyfile, "ncpmount_options", "b", NULL);
    cmd_params.m = g_key_file_get_boolean (keyfile, "ncpmount_options", "m", NULL);


    g_key_file_free(keyfile);
}



/**
*   Save cmd_params to gkeyfile
*/
void save_cmdparams_to_file (const gchar *filename)
{
    GKeyFile *keyfile;
    gsize length;
    gchar *file_data;
    FILE *saved;

    keyfile = g_key_file_new ();

    g_key_file_set_comment(keyfile, NULL, NULL, "gncpmount - config file\n-------------------------\nGenerated file - please do not modify it manualy!", NULL);



    if(cmd_params.username != NULL)
    {
        g_key_file_set_string (keyfile, "ncpmount_options", "username", cmd_params.username);
    }

/*
    if(cmd_params.password != NULL)
    {
        g_key_file_set_string (keyfile, "ncpmount_options", "password", cmd_params.password);
    }
*/

    if(cmd_params.server != NULL)
    {
        g_key_file_set_string (keyfile, "ncpmount_options", "server", cmd_params.server);

    }

    if(cmd_params.dns_name != NULL)
    {
        g_key_file_set_string (keyfile, "ncpmount_options", "dns_name", cmd_params.dns_name);
    }

    if(cmd_params.volume != NULL)
    {
        g_key_file_set_string (keyfile, "ncpmount_options", "volume", cmd_params.volume);
    }

    if(cmd_params.uid != NULL)
    {
        g_key_file_set_string (keyfile, "ncpmount_options", "uid", cmd_params.uid);
    }

    if(cmd_params.gid != NULL)
    {
        g_key_file_set_string (keyfile, "ncpmount_options", "gid", cmd_params.gid);
    }

    if(cmd_params.fmode != NULL)
    {
        g_key_file_set_string (keyfile, "ncpmount_options", "fmode", cmd_params.fmode);
    }

    if(cmd_params.dmode != NULL)
    {
        g_key_file_set_string (keyfile, "ncpmount_options", "dmode", cmd_params.dmode);
    }

    if(cmd_params.time_out != NULL)
    {
        g_key_file_set_string (keyfile, "ncpmount_options", "time_out", cmd_params.time_out);
    }

    if(cmd_params.retry_count != NULL)
    {
        g_key_file_set_string (keyfile, "ncpmount_options", "retry_count", cmd_params.retry_count);
    }

    if(cmd_params.level != NULL)
    {
        g_key_file_set_string (keyfile, "ncpmount_options", "level", cmd_params.level);
    }

    if(cmd_params.charset != NULL)
    {
        g_key_file_set_string (keyfile, "ncpmount_options", "charset", cmd_params.charset);
    }

    if(cmd_params.codepage != NULL)
    {
        g_key_file_set_string (keyfile, "ncpmount_options", "codepage", cmd_params.codepage);
    }

    if(cmd_params.extraoptions != NULL)
    {
        g_key_file_set_string (keyfile, "ncpmount_options", "extraoptions", cmd_params.extraoptions);
    }

    if(cmd_params.mount_point != NULL)
    {
        g_key_file_set_string (keyfile, "ncpmount_options", "mount_point", cmd_params.mount_point);
    }



    g_key_file_set_boolean (keyfile, "ncpmount_options", "C",  cmd_params.C);
    g_key_file_set_boolean (keyfile, "ncpmount_options", "n",  cmd_params.n);
    g_key_file_set_boolean (keyfile, "ncpmount_options", "s",  cmd_params.s);
    g_key_file_set_boolean (keyfile, "ncpmount_options", "b",  cmd_params.b);
    g_key_file_set_boolean (keyfile, "ncpmount_options", "m",  cmd_params.m);



    /* Key file to gchar* */
    file_data = g_key_file_to_data(keyfile, &length, NULL);

    saved = fopen (filename, "w");
    if(saved != NULL)
    {
        fprintf(saved, "%s", file_data);
    }
    else
        show_message( GTK_MESSAGE_ERROR,"ERROR: While opening keyfile", "","", "");

    fclose(saved);

    g_free(file_data);
    g_key_file_free(keyfile);
}
