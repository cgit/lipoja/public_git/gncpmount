 /*
  *  dialogs.h
  *
  *  This file is part of gncpmount.
  *
  *  gncpmount is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  *
  *  gncpmount is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License
  *  along with gncpmount.  If not, see <http://www.gnu.org/licenses/>.
  *
  *  Author: Jan Lipovsky <janlipovsky@gmail.com>
  *
  */
#ifndef GNCPMOUNT_DIALOGS_H
#define GNCPMOUNT_DIALOGS_H

#include "globals.h"

/** Shows options dialog */
void show_options_dialog ();

/** Shows error, info or warning message */
void show_message (GtkMessageType type, const gchar *format, gchar *msgtxt, const gchar *format_sec, gchar *msgtxt_sec);

/** Function shows ncpmount command with set options */
void show_ncpmount_command ();

/** Function shows about dialog */
void show_about ();

#endif

