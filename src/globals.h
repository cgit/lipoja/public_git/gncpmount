 /*
  *  globals.h
  *
  *  This file is part of gncpmount.
  *
  *  gncpmount is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  *
  *  gncpmount is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License
  *  along with gncpmount.  If not, see <http://www.gnu.org/licenses/>.
  *
  *  Author: Jan Lipovsky <janlipovsky@gmail.com>
  *
  */
#ifndef GNCPMOUNT_GLOBALS_H
#define GNCPMOUNT_GLOBALS_H

#include <gtk/gtk.h>

/* gettext - lokalizace */
#include <locale.h>
#include <libintl.h>
#define _(string) gettext(string)

#define PROG_VERSION "version 0.0.4"
#define GUI_TITLE "gncpmount"

/* CB - im using codeblocks */
#ifdef CB
    #define PACKAGE "gncpmount"
    #define GNOMELOCALEDIR "/usr/share/locale"
    #define NCPMOUNT_PATH "/usr/bin/ncpmount"
#endif


/**
* Define type TWindow - structure of important widgets in gui
*/
typedef struct gui_struct
{
    GtkWidget *win;
    GtkWidget *menu;
    GtkWidget *vbox;
    GtkWidget *table;

    GtkWidget *check_n;

    GtkWidget *btnbox;

    GtkWidget *notebook;

    /* Buttons */
    GtkWidget *btn_close;
    GtkWidget *btn_mount;
    GtkWidget *btn_browse;

    gboolean   btn_mount_enable;

    /* Entry */
    GtkWidget *entry_username;
    GtkWidget *entry_password;
    GtkWidget *entry_server;
    GtkWidget *entry_mount_point;
    GtkWidget *entry_dns_server;
} TWindow;

/** Structure of widgets used in GUI */
TWindow gui;



/**
* Define type TWindow - structure of important widgets in gui
*/
typedef struct opt_struct
{
    GtkWidget *dialog;

    /* Entry */
    GtkWidget *entry_codepage;
    GtkWidget *entry_charset;
    GtkWidget *entry_volume;
    GtkWidget *entry_uid;
    GtkWidget *entry_gid;
    GtkWidget *entry_fmode;
    GtkWidget *entry_dmode;
    GtkWidget *entry_timeout;
    GtkWidget *entry_retrycount;
    GtkWidget *entry_extraoptions;

    /* ComboBox */
    GtkWidget *combo_signature;

    /* Check box */
    GtkWidget *check_C;
    GtkWidget *check_s;
    GtkWidget *check_m;
    GtkWidget *check_b;
}TODialog;

/** Structure of widgets used in GUI - Optionns dialog */
TODialog options;



/**
* Parameters which are appent to the ncpmount command
*/
typedef struct params
{
    gchar *username;        /* user name sent to server */
    gchar *password;        /* use this password */
    gchar *server;          /* server name to be used */
    gchar *dns_name;        /* DNS server name to be used when mounting over TCP or UDP */
    gchar *mount_point;     /* mount point directory */

    gchar *volume;          /* Volume to mount, for NFS re-eport */
    gchar *uid;             /* uid the mounted files get */
    gchar *gid;             /* gid the mounted files get */
    gchar *fmode;           /* permission the files get (octal notation) */
    gchar *dmode;           /* permission the dirs get (octal notation) */
    gchar *time_out;        /* waiting time (in 1/100s) to wait for an answer from the server. Default: 60 */
    gchar *retry_count;     /* number of retry attempts. Default: 5 */
    gchar *level;           /* signature level */
    gchar *charset;         /* charset used for input and display */
    gchar *codepage;        /* codepage used on volume, including letters 'cp' */

    gchar *extraoptions;    /* extra options - type any parameter which is not in the options dialog */

    /*gchar *cuid; */      /* uid to identify the connection to mount on - Only makes sense for root */

    gboolean C;             /* don't convert password to uppercase */
    gboolean n;             /* do not use any password */
    gboolean s;             /* enable renaming/deletion of read-only files */
    gboolean b;             /* force bindery login to NDS servers */
    gboolean m;             /* allow multiple logins to server */

} TParameters;

/** Optionns and mount-point */
TParameters cmd_params;


/** Sets given parameter whith given value */
void set_parameter_text (GtkWidget *entry, gchar **param);


/** Sets given parameter whith given value */
void set_parameter_bool (GtkWidget *checkbox, gboolean *param);


/**
*   Return ncpmount command - set length = NULL
*   if you do not wannt to know length of returnet string
*/
gchar * get_ncpmount_command (gboolean whith_password, gint *length);


/** Clear all parameter in cmd_params*/
void clear_cmdparams ();

#endif
