 /*
  *  keyfile.h
  *
  *  This file is part of gncpmount.
  *
  *  gncpmount is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  *
  *  gncpmount is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License
  *  along with gncpmount.  If not, see <http://www.gnu.org/licenses/>.
  *
  *  Author: Jan Lipovsky <janlipovsky@gmail.com>
  *
  */
#ifndef GNCPMOUNT_KEYFILE_H
#define GNCPMOUNT_KEYFILE_H

#include "globals.h"


/** Load cmd_params from file */
void load_cmdparams_from_file (const gchar *filename);

/** Save cmd_params to file */
void save_cmdparams_to_file (const gchar *filename);
#endif
