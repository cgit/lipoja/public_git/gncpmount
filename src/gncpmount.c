 /*
  *  gncpmount.c
  *
  *  This file is part of gncpmount.
  *
  *  gncpmount is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  *
  *  gncpmount is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License
  *  along with gncpmount.  If not, see <http://www.gnu.org/licenses/>.
  *
  *  Author: Jan Lipovsky <janlipovsky@gmail.com>
  *
  */
#include "globals.h"
#include "keyfile.h"
#include "ncpwrapper.h"
#include "dialogs.h"

#include "logo.xpm.h"

#define WIDTH -1
#define HEIGHT -1



/**
* Load saved options to GUI
*/
static void
set_gui_from_cmdparams ()
{

    if(cmd_params.username != NULL)
        gtk_entry_set_text(GTK_ENTRY(gui.entry_username), g_shell_unquote(cmd_params.username, NULL));
    else
        gtk_entry_set_text(GTK_ENTRY(gui.entry_username), "");

    if(cmd_params.password != NULL)
        gtk_entry_set_text(GTK_ENTRY(gui.entry_password), g_shell_unquote(cmd_params.password, NULL));
    else
        gtk_entry_set_text(GTK_ENTRY(gui.entry_password), "");

    if(cmd_params.server != NULL)
        gtk_entry_set_text(GTK_ENTRY(gui.entry_server), g_shell_unquote(cmd_params.server, NULL));
    else
        gtk_entry_set_text(GTK_ENTRY(gui.entry_server), "");

    if(cmd_params.dns_name != NULL)
        gtk_entry_set_text(GTK_ENTRY(gui.entry_dns_server), g_shell_unquote(cmd_params.dns_name, NULL));
    else
        gtk_entry_set_text(GTK_ENTRY(gui.entry_dns_server), "");

    if(cmd_params.mount_point != NULL)
        gtk_entry_set_text(GTK_ENTRY(gui.entry_mount_point), g_shell_unquote(cmd_params.mount_point, NULL));
    else
        gtk_entry_set_text(GTK_ENTRY(gui.entry_mount_point), "");

    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (gui.check_n), cmd_params.n);
}



/**
* Save options set in GUI by user to cmd_params
*/
static void
save_gui_to_cmdparams()
{
    set_parameter_text ((gui.entry_dns_server), &cmd_params.dns_name);
    set_parameter_text ((gui.entry_mount_point), &cmd_params.mount_point);
    set_parameter_text ((gui.entry_password), &cmd_params.password);
    set_parameter_text ((gui.entry_server), &cmd_params.server);
    set_parameter_text ((gui.entry_username), &cmd_params.username);

    /* Checkboxes */
    set_parameter_bool ((gui.check_n), &cmd_params.n);
}



/**
* Check length of all entry and if they are filled enables mount button
*/
static void
enable_mount_button ()
{
    gint tmp = ( gtk_entry_get_text_length (GTK_ENTRY(gui.entry_username)) *
                 gtk_entry_get_text_length (GTK_ENTRY(gui.entry_password)) *
                 gtk_entry_get_text_length (GTK_ENTRY(gui.entry_mount_point)));

    if ( (tmp * gtk_entry_get_text_length (GTK_ENTRY(gui.entry_server)) != 0) ||
            (tmp * gtk_entry_get_text_length (GTK_ENTRY(gui.entry_dns_server))) != 0)
    {
        if(!gui.btn_mount_enable)
        {
            gui.btn_mount_enable = TRUE;
            gtk_widget_set_sensitive (gui.btn_mount, TRUE);
        }
    }
    else
    {
        if(gui.btn_mount_enable)
        {
            gui.btn_mount_enable = FALSE;
            gtk_widget_set_sensitive (gui.btn_mount, FALSE);
        }
    }
}



/**
* Create Open folder dialog and let user select folder
* for mount point
*/
static void
open_folder ()
{
    GtkWidget *dialog;
    dialog = gtk_file_chooser_dialog_new (_("Select folder - mount point"),
                                          GTK_WINDOW (gui.win),
                                          GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER,
                                          GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                                          GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
                                          NULL);
    if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT)
    {
        gchar *folder;
        folder = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog));

        gtk_entry_set_text (GTK_ENTRY (gui.entry_mount_point), folder);
        g_free (folder);
    }
    gtk_widget_destroy (dialog);

}



/**
* Create Open file dialog
* and load cmd_params from file
*/
static void
open_file ()
{
    GtkWidget *dialog;
    GtkFileFilter *fgnc, *fall;

    dialog = gtk_file_chooser_dialog_new (_("Load options from file"),
                                          GTK_WINDOW (gui.win),
                                          GTK_FILE_CHOOSER_ACTION_OPEN,
                                          GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                                          GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
                                          NULL);

    fgnc = gtk_file_filter_new();
    gtk_file_filter_set_name(fgnc,"gncpmout (*.gnc)");
    gtk_file_filter_add_pattern(fgnc,"*.gnc");

    gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(dialog),fgnc);

    fall = gtk_file_filter_new();
    gtk_file_filter_set_name(fall,"All files (*.*)");
    gtk_file_filter_add_pattern(fall,"*");
    gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(dialog),fall);

    if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT)
    {
        char *filename;
        filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog));
        load_cmdparams_from_file (filename);

        /* Set gui */
        set_gui_from_cmdparams();

        g_free (filename);
    }
    gtk_widget_destroy (dialog);
}



/**
* Save cmd_params to default file
*/
static void
set_default ()
{
    gchar *path = g_strconcat(g_get_home_dir(),"/.gncpmount", NULL);
    gchar *filename = g_strconcat(path, "/default", NULL);


    if (g_mkdir_with_parents(path,448) < 0) /* Octal - 700*/
    {
        show_message(GTK_MESSAGE_ERROR,_("ERROR: Set as default"),"",_("Can't create directory: ~/.gncpmount"),"");
    }

    save_gui_to_cmdparams();
    save_cmdparams_to_file (filename);

    show_message(GTK_MESSAGE_INFO,_("Set as default"),"",_("Current state of GUI was set as default."),"");

    g_free (path);
    g_free (filename);
}



/**
* Create Save file dialog
* and save cmd_params to file
*/
static void
save_file ()
{
    GtkWidget *dialog;
    GtkFileFilter *fgnc, *fall;

    dialog = gtk_file_chooser_dialog_new (_("Save options to file"),
                                          GTK_WINDOW (gui.win),
                                          GTK_FILE_CHOOSER_ACTION_SAVE,
                                          GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                                          GTK_STOCK_SAVE, GTK_RESPONSE_ACCEPT,
                                          NULL);

    gtk_file_chooser_set_do_overwrite_confirmation (GTK_FILE_CHOOSER (dialog), TRUE);
    /*
    if (user_edited_a_new_document)
    {
        gtk_file_chooser_set_current_folder (GTK_FILE_CHOOSER (dialog), default_folder_for_saving);
        gtk_file_chooser_set_current_name (GTK_FILE_CHOOSER (dialog), "Untitled document");
    }
    else
        gtk_file_chooser_set_filename (GTK_FILE_CHOOSER (dialog), filename_for_existing_document);
        */

    fgnc = gtk_file_filter_new();
    gtk_file_filter_set_name(fgnc,"gncpmout (*.gnc)");
    gtk_file_filter_add_pattern(fgnc,"*.gnc");

    gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(dialog),fgnc);

    fall = gtk_file_filter_new();
    gtk_file_filter_set_name(fall,"All files (*.*)");
    gtk_file_filter_add_pattern(fall,"*");
    gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(dialog),fall);


    if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT)
    {
        char *filename;
        filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog));

        save_gui_to_cmdparams();
        save_cmdparams_to_file (filename);

        g_free (filename);
    }
    gtk_widget_destroy (dialog);
}



/**
* Call ncpmount command
*/
static void
call_ncpmount ()
{
    save_gui_to_cmdparams();
    run_ncpmount();
}



/**
* Set true if -P (password) is used or not
*/
static void
passwd_usage ()
{
    cmd_params.n = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (gui.check_n));

    gtk_editable_set_editable (GTK_EDITABLE(gui.entry_password), !cmd_params.n);

    if(cmd_params.n)
    {
        gtk_entry_set_visibility (GTK_ENTRY(gui.entry_password), TRUE);
        set_parameter_text (gui.entry_password, &(cmd_params.password));
        gtk_entry_set_text (GTK_ENTRY(gui.entry_password), _("no password will be used"));
    }
    else
    {
        gtk_entry_set_visibility (GTK_ENTRY(gui.entry_password), FALSE);
        if(cmd_params.password != NULL)
            gtk_entry_set_text (GTK_ENTRY(gui.entry_password), cmd_params.password);
        else
            gtk_entry_set_text (GTK_ENTRY(gui.entry_password), "");
    }
}



/**
* Open Options dialog
*/
static void
run_options_dialog ()
{
    show_options_dialog();
}



/**
* Open Show command dialog
*/
static void
run_command_dialog ()
{
    save_gui_to_cmdparams();
    show_ncpmount_command();
}



/**
* Clear cmd_params and set gui to default
*/
static void
clear_gui ()
{
    clear_cmdparams();
    set_gui_from_cmdparams();
}



int main (int argc, char *argv[])
{
    GtkWidget *label = NULL;
    GtkWidget *tmp = NULL;
    GtkWidget *hbox = NULL;
    GtkWidget *halign = NULL;
    GtkWidget *logo = NULL;
    GtkWidget *menu = NULL;
    GtkWidget *submenu = NULL;
    GtkWidget *menuitem = NULL;

    gint i; //for
    GdkPixbuf *xpm = NULL;

    GList *icon_list = NULL;

    gchar *keyfile_default = g_strconcat(g_get_home_dir(),"/.gncpmount/default", NULL);

    setlocale(LC_ALL, "");
    bindtextdomain(PACKAGE, GNOMELOCALEDIR);
    textdomain(PACKAGE);
    bind_textdomain_codeset (PACKAGE, "UTF-8");

    /* Initialize GTK+ */
    g_log_set_handler ("Gtk", G_LOG_LEVEL_WARNING, (GLogFunc) gtk_false, NULL);
    gtk_init (&argc, &argv);
    g_log_set_handler ("Gtk", G_LOG_LEVEL_WARNING, g_log_default_handler, NULL);

    /* Create the main window */
    gui.win = gtk_window_new (GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title (GTK_WINDOW (gui.win), GUI_TITLE );
    gtk_window_set_position (GTK_WINDOW (gui.win), GTK_WIN_POS_CENTER);
    gtk_window_set_default_size (GTK_WINDOW (gui.win), WIDTH, HEIGHT);
    gtk_window_set_resizable (GTK_WINDOW (gui.win), FALSE);

    gchar *icon_names[5] = {"/usr/share/icons/hicolor/24x24/apps/gncpmount.png",
                            "/usr/share/icons/hicolor/32x32/apps/gncpmount.png",
                            "/usr/share/icons/hicolor/48x48/apps/gncpmount.png",
                            "/usr/share/icons/hicolor/64x64/apps/gncpmount.png",
                            "/usr/share/icons/hicolor/96x96/apps/gncpmount.png"};

    for(i = 0; i < 5; i++)
    {
        GdkPixbuf *tmp =  gdk_pixbuf_new_from_file(icon_names[i], NULL);
        icon_list = g_list_append (icon_list, tmp);
    }

    gtk_window_set_icon_list (GTK_WINDOW(gui.win), icon_list);


    gtk_widget_realize (gui.win);
    g_signal_connect (gui.win, "destroy", gtk_main_quit, NULL);


    tmp = gtk_vbox_new (FALSE, 1);
    gtk_container_add (GTK_CONTAINER (gui.win), tmp);

    /* Create menu bar*/
    gui.menu = gtk_menu_bar_new();
    gtk_box_pack_start (GTK_BOX (tmp), gui.menu, FALSE, TRUE, 0);

    /* File menu */
    menu = gtk_menu_item_new_with_mnemonic (_("_File"));
    gtk_menu_shell_append (GTK_MENU_SHELL(gui.menu), menu);
    gtk_widget_show(menu);

    /*-- Create File submenu  --*/
    submenu = gtk_menu_new();
    gtk_menu_item_set_submenu(GTK_MENU_ITEM(menu), submenu);

    menuitem = gtk_menu_item_new_with_mnemonic (_("_New"));
    gtk_menu_shell_append(GTK_MENU_SHELL(submenu), menuitem);
    g_signal_connect ( (menuitem), "activate", clear_gui, NULL);
    gtk_widget_show (menuitem);

    menuitem = gtk_menu_item_new_with_mnemonic (_("_Load from file"));
    gtk_menu_shell_append(GTK_MENU_SHELL(submenu), menuitem);
    g_signal_connect ( (menuitem), "activate", open_file, NULL);
    gtk_widget_show (menuitem);

    menuitem = gtk_menu_item_new_with_mnemonic (_("_Save as..."));
    gtk_menu_shell_append(GTK_MENU_SHELL(submenu), menuitem);
    g_signal_connect ( (menuitem), "activate", save_file, NULL);
    gtk_widget_show (menuitem);

    menuitem = gtk_menu_item_new_with_mnemonic (_("_Set as default"));
    gtk_menu_shell_append(GTK_MENU_SHELL(submenu), menuitem);
    g_signal_connect ( (menuitem), "activate", set_default, NULL);
    gtk_widget_show (menuitem);

    menuitem = gtk_separator_menu_item_new ();
    gtk_menu_shell_append(GTK_MENU_SHELL(submenu), menuitem);
    gtk_widget_show (menuitem);

    menuitem = gtk_menu_item_new_with_mnemonic (_("_Quit"));
    gtk_menu_shell_append(GTK_MENU_SHELL(submenu), menuitem);
    g_signal_connect ( (menuitem), "activate", gtk_main_quit, NULL); //GTK_OBJECT
    gtk_widget_show (menuitem);


    /* Tools menu */
    menu = gtk_menu_item_new_with_mnemonic (_("_Tools"));
    gtk_menu_shell_append (GTK_MENU_SHELL(gui.menu), menu);
    gtk_widget_show(menu);

    submenu = gtk_menu_new();
    gtk_menu_item_set_submenu(GTK_MENU_ITEM(menu), submenu);

    menuitem = gtk_menu_item_new_with_mnemonic (_("_Options"));
    gtk_menu_shell_append(GTK_MENU_SHELL(submenu), menuitem);
    g_signal_connect ( (menuitem), "activate", run_options_dialog, NULL);
    gtk_widget_show (menuitem);

    menuitem = gtk_menu_item_new_with_mnemonic (_("Show _command"));
    gtk_menu_shell_append(GTK_MENU_SHELL(submenu), menuitem);
    g_signal_connect ( (menuitem), "activate", run_command_dialog, NULL);
    gtk_widget_show (menuitem);


    /* Help menu */
    menu = gtk_menu_item_new_with_mnemonic (_("_Help"));
    gtk_menu_shell_append (GTK_MENU_SHELL(gui.menu), menu);
    gtk_widget_show(menu);

    submenu = gtk_menu_new();
    gtk_menu_item_set_submenu(GTK_MENU_ITEM(menu), submenu);

    menuitem = gtk_menu_item_new_with_mnemonic (_("_About..."));
    gtk_menu_shell_append(GTK_MENU_SHELL(submenu), menuitem);
    g_signal_connect ( (menuitem), "activate", show_about, NULL);
    gtk_widget_show (menuitem);

    /* Create a vertical box */
    gui.vbox = gtk_vbox_new (FALSE, 6);
    gtk_box_pack_start (GTK_BOX (tmp), gui.vbox,  FALSE, TRUE, 0);

    gtk_container_set_border_width (GTK_CONTAINER (gui.vbox), 10);

    xpm = gdk_pixbuf_new_from_xpm_data(gncpmount_xpm);

    logo = gtk_image_new_from_pixbuf(xpm);
    gtk_box_pack_start (GTK_BOX (gui.vbox), logo, TRUE, TRUE, 0);

    /* Create a notebook */
    gui.notebook = gtk_notebook_new ();
    gtk_notebook_set_tab_pos (GTK_NOTEBOOK (gui.notebook), GTK_POS_TOP);
    gtk_box_pack_start (GTK_BOX (gui.vbox), gui.notebook, TRUE, TRUE, 0);
    gtk_widget_set_size_request(gui.notebook, WIDTH,-1);


    /* Page Login */

    /* Create table */
    gui.table = gtk_table_new (3, 2, FALSE);
    label = gtk_label_new (_("Login"));
    gtk_notebook_prepend_page (GTK_NOTEBOOK (gui.notebook), gui.table, label);

    /* Labels */
    halign = gtk_alignment_new(0, 0, 0, 1);

    label = gtk_label_new (_("Username [-U]:"));
    gtk_container_add(GTK_CONTAINER(halign), label);
    gtk_table_attach(GTK_TABLE(gui.table), halign, 0, 1, 0, 1,  GTK_FILL , GTK_FILL | GTK_EXPAND, 4, 0);


    gui.entry_username = gtk_entry_new ();
    g_signal_connect (G_OBJECT (gui.entry_username), "changed", G_CALLBACK (enable_mount_button), NULL);
    gtk_table_attach_defaults (GTK_TABLE (gui.table),  gui.entry_username, 1, 2, 0, 1);

    halign = gtk_alignment_new(0, 0, 0, 1);
    label = gtk_label_new (_("Password [-P]:"));
    gtk_container_add(GTK_CONTAINER(halign), label);
    gtk_table_attach(GTK_TABLE(gui.table), halign, 0, 1, 1, 2,  GTK_FILL, GTK_FILL | GTK_EXPAND, 4, 0);


    gui.entry_password = gtk_entry_new ();
    gtk_entry_set_visibility (GTK_ENTRY(gui.entry_password), FALSE);
    g_signal_connect (G_OBJECT (gui.entry_password), "changed", G_CALLBACK (enable_mount_button), NULL);
    gtk_table_attach_defaults (GTK_TABLE (gui.table),  gui.entry_password, 1, 2, 1, 2);

    /* Chcekbox */
    gui.check_n = gtk_check_button_new_with_label (_("Do not use any password [-n]"));
    g_signal_connect (G_OBJECT (gui.check_n), "toggled", G_CALLBACK (passwd_usage), NULL);
    gtk_table_attach_defaults (GTK_TABLE (gui.table), gui.check_n, 1, 2, 2, 3);



    /* Page SERVER */
    gui.table = gtk_table_new (4, 2, FALSE);

    label = gtk_label_new (_("Server"));
    gtk_notebook_append_page (GTK_NOTEBOOK (gui.notebook), gui.table, label);

    halign = gtk_alignment_new(0, 0, 0, 1);
    label = gtk_label_new (_("Mount point:"));
    gtk_container_add(GTK_CONTAINER(halign), label);
    gtk_table_attach(GTK_TABLE(gui.table), halign, 0, 1, 0, 1,  GTK_FILL , GTK_FILL | GTK_EXPAND, 6, 0);

    hbox = gtk_hbox_new (FALSE, 2);
    gtk_table_attach_defaults (GTK_TABLE (gui.table), hbox, 1, 2, 0, 1);
    gui.entry_mount_point = gtk_entry_new ();
    g_signal_connect (G_OBJECT (gui.entry_mount_point), "changed", G_CALLBACK (enable_mount_button), NULL);
    gtk_container_add(GTK_CONTAINER(hbox), gui.entry_mount_point);

    gui.btn_browse = gtk_button_new_from_stock (GTK_STOCK_OPEN);
    gtk_button_set_use_stock (GTK_BUTTON (gui.btn_browse), TRUE);
    g_signal_connect (gui.btn_browse, "clicked", G_CALLBACK (open_folder), NULL);
    gtk_container_add(GTK_CONTAINER(hbox), gui.btn_browse);

    tmp = gtk_hseparator_new();
    gtk_table_attach (GTK_TABLE(gui.table), tmp, 0, 2, 1, 2,  GTK_FILL | GTK_EXPAND, GTK_FILL | GTK_EXPAND, 6, 6);


    halign = gtk_alignment_new(0, 0, 0, 1);
    label = gtk_label_new (_("Server [-S]:"));
    gtk_container_add(GTK_CONTAINER(halign), label);
    gtk_table_attach(GTK_TABLE(gui.table), halign, 0, 1, 2, 3,  GTK_FILL, GTK_FILL | GTK_EXPAND, 6, 0);

    gui.entry_server = gtk_entry_new ();
    g_signal_connect (G_OBJECT (gui.entry_server), "changed", G_CALLBACK (enable_mount_button), NULL);
    gtk_table_attach_defaults (GTK_TABLE (gui.table),  gui.entry_server, 1, 2, 2, 3);


    halign = gtk_alignment_new(0, 0, 0, 1);
    label = gtk_label_new (_("DNS name [-A]:"));
    gtk_container_add(GTK_CONTAINER(halign), label);
    gtk_table_attach(GTK_TABLE(gui.table), halign, 0, 1, 3, 4,  GTK_FILL, GTK_FILL | GTK_EXPAND, 6, 0);

    gui.entry_dns_server = gtk_entry_new ();
    g_signal_connect (G_OBJECT (gui.entry_dns_server), "changed", G_CALLBACK (enable_mount_button), NULL);
    gtk_table_attach_defaults (GTK_TABLE (gui.table),  gui.entry_dns_server, 1, 2, 3, 4);



    /* Button box */
    gui.btnbox = gtk_hbutton_box_new();
    gtk_button_box_set_layout (GTK_BUTTON_BOX(gui.btnbox), GTK_BUTTONBOX_END);
    gtk_box_pack_start (GTK_BOX (gui.vbox), gui.btnbox, TRUE, TRUE, 0);

    /* Buttons */
    gui.btn_mount = gtk_button_new_with_label  (_("Mount"));
    g_signal_connect (gui.btn_mount, "clicked", G_CALLBACK(call_ncpmount), NULL);
    gtk_box_pack_start (GTK_BOX (gui.btnbox), gui.btn_mount, TRUE, TRUE, 0);


    gui.btn_mount_enable = FALSE;
    gtk_widget_set_sensitive (gui.btn_mount, FALSE);

    gui.btn_close = gtk_button_new_from_stock (GTK_STOCK_CLOSE);
    g_signal_connect (gui.btn_close, "clicked", gtk_main_quit, NULL);
    gtk_box_pack_start (GTK_BOX (gui.btnbox), gui.btn_close, TRUE, TRUE, 0);


    /* Fill in informations from cmd_params to widgets */
    clear_cmdparams ();

    /* Try to load default settings */
    if (g_file_test(keyfile_default, G_FILE_TEST_EXISTS))
        load_cmdparams_from_file (keyfile_default);

    /* Set gui */
    set_gui_from_cmdparams();

    /* Enter the main loop */
    gtk_widget_show_all (gui.win);
    gtk_main ();

    g_free(keyfile_default);
    return 0;
}
