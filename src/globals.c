 /*
  *  globals.c
  *
  *  This file is part of gncpmount.
  *
  *  gncpmount is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  *
  *  gncpmount is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License
  *  along with gncpmount.  If not, see <http://www.gnu.org/licenses/>.
  *
  *  Author: Jan Lipovsky <janlipovsky@gmail.com>
  *
  */
#include "globals.h"


/**
*   Set text value in given parameter
*/
void set_parameter_text (GtkWidget *entry, gchar **param)
{
    gchar *tmp;
    tmp = g_strdup(gtk_entry_get_text (GTK_ENTRY(entry)));

    g_strstrip(tmp);

    if(!g_strcmp0(tmp,""))
    {
        g_free(tmp);
        tmp = g_strdup(NULL);;
    }

    g_free(*param);

    if(tmp != NULL)
        *param = g_shell_quote(tmp);
    else
        *param = g_strdup(tmp);

    g_free(tmp);
}



/**
*   Set boolean value in given parameter
*/
void set_parameter_bool (GtkWidget *checkbox, gboolean *param)
{
    *param = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (checkbox));
}



/**
* Return ncpmount command with options and mountpoint
* if length != NULL return length of ret string
*/
gchar *get_ncpmount_command (gboolean whith_password, gint *length)
{
    GString *command;
    gchar *ret;

    command = g_string_new("ncpmount");

    if(cmd_params.username != NULL)
    {
        command = g_string_append (command, " -U ");
        command = g_string_append (command, cmd_params.username);
    }

    if(whith_password)
    {
        if(cmd_params.password != NULL && !cmd_params.n)
        {
            command = g_string_append (command, " -P ");
            command = g_string_append (command, cmd_params.password);
        }
    }

    if(cmd_params.server != NULL)
    {
        command = g_string_append (command, " -S ");
        command = g_string_append (command, cmd_params.server);
    }

    if(cmd_params.dns_name != NULL)
    {
        command = g_string_append (command, " -A ");
        command = g_string_append (command, cmd_params.dns_name);
    }

    if(cmd_params.volume != NULL)
    {
        command = g_string_append (command, " -V ");
        command = g_string_append (command, cmd_params.volume);
    }

    if(cmd_params.uid != NULL)
    {
        command = g_string_append (command, " -u ");
        command = g_string_append (command, cmd_params.uid);
    }

    if(cmd_params.gid != NULL)
    {
        command = g_string_append (command, " -g ");
        command = g_string_append (command, cmd_params.gid);
    }

    if(cmd_params.fmode != NULL)
    {
        command = g_string_append (command, " -f ");
        command = g_string_append (command, cmd_params.fmode);
    }

    if(cmd_params.dmode != NULL)
    {
        command = g_string_append (command, " -d ");
        command = g_string_append (command, cmd_params.dmode);
    }

    if(cmd_params.time_out != NULL)
    {
        command = g_string_append (command, " -t ");
        command = g_string_append (command, cmd_params.time_out);
    }

    if(cmd_params.retry_count != NULL)
    {
        command = g_string_append (command, " -r ");
        command = g_string_append (command, cmd_params.retry_count);
    }

    if(cmd_params.level != NULL)
    {
        command = g_string_append (command, " -i ");
        command = g_string_append (command, cmd_params.level);
    }

    if(cmd_params.charset != NULL)
    {
        command = g_string_append (command, " -y ");
        command = g_string_append (command, cmd_params.charset);
    }

    if(cmd_params.codepage != NULL)
    {
        command = g_string_append (command, " -p ");
        command = g_string_append (command, cmd_params.codepage);
    }

    /* Extra options */
    if(cmd_params.extraoptions != NULL)
    {
        command = g_string_append (command, " ");
        command = g_string_append (command, g_shell_unquote(cmd_params.extraoptions, NULL));
    }

    if(cmd_params.C)
    {
        command = g_string_append (command, " -C ");
    }

    if(cmd_params.n)
    {
        command = g_string_append (command, " -n ");
    }

    if(cmd_params.s)
    {
        command = g_string_append (command, " -s ");
    }

    if(cmd_params.b)
    {
        command = g_string_append (command, " -b ");
    }

    if(cmd_params.m)
    {
        command = g_string_append (command, " -m ");
    }


    command = g_string_append (command, " ");

    /* Mount point on the end */
    if(cmd_params.mount_point != NULL)
        command = g_string_append (command, cmd_params.mount_point);

    if(length != NULL)
        *length = command->len;

    ret = command->str;
    g_string_free(command, FALSE);

    return ret;
}



/**
* Clear all parameter in cmd_params
*/
void clear_cmdparams ()
{
    if(cmd_params.username != NULL)
    {
        g_free(cmd_params.username);
        cmd_params.username = NULL;
    }

    if(cmd_params.password != NULL)
    {
        g_free(cmd_params.password);
        cmd_params.password = NULL;
    }

    if(cmd_params.server != NULL)
    {
        g_free(cmd_params.server);
        cmd_params.server = NULL;
    }

    if(cmd_params.dns_name != NULL)
    {
        g_free(cmd_params.dns_name);
        cmd_params.dns_name = NULL;
    }

    if(cmd_params.volume != NULL)
    {
        g_free(cmd_params.volume);
        cmd_params.volume = NULL;
    }

    if(cmd_params.uid != NULL)
    {
        g_free(cmd_params.uid);
        cmd_params.uid = NULL;
    }

    if(cmd_params.gid != NULL)
    {
        g_free(cmd_params.gid);
        cmd_params.gid = NULL;
    }

    if(cmd_params.fmode != NULL)
    {
        g_free(cmd_params.fmode);
        cmd_params.fmode = NULL;
    }

    if(cmd_params.dmode != NULL)
    {
        g_free(cmd_params.dmode);
        cmd_params.dmode = NULL;
    }

    if(cmd_params.time_out != NULL)
    {
        g_free(cmd_params.time_out);
        cmd_params.time_out = NULL;
    }

    if(cmd_params.retry_count != NULL)
    {
        g_free(cmd_params.retry_count);
        cmd_params.retry_count = NULL;
    }

    if(cmd_params.level != NULL)
    {
        g_free(cmd_params.level);
        cmd_params.level = NULL;
    }

    if(cmd_params.charset != NULL)
    {
        g_free(cmd_params.charset);
        cmd_params.charset = NULL;
    }

    if(cmd_params.codepage != NULL)
    {
        g_free(cmd_params.codepage);
        cmd_params.codepage = NULL;
    }

    if(cmd_params.mount_point != NULL)
    {
        g_free(cmd_params.mount_point);
        cmd_params.mount_point = NULL;
    }

    if(cmd_params.extraoptions != NULL)
    {
        g_free(cmd_params.extraoptions);
        cmd_params.extraoptions = NULL;
    }

    cmd_params.C = FALSE;
    cmd_params.n = FALSE;
    cmd_params.s = FALSE;
    cmd_params.b = FALSE;
    cmd_params.m = FALSE;
}

