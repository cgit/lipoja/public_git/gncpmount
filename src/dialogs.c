 /*
  *  dialogs.c
  *
  *  This file is part of gncpmount.
  *
  *  gncpmount is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  *
  *  gncpmount is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License
  *  along with gncpmount.  If not, see <http://www.gnu.org/licenses/>.
  *
  *  Author: Jan Lipovsky <janlipovsky@gmail.com>
  *
  */
#include "dialogs.h"
#include "about.xmp.h"


/** Signature level type */
typedef enum lvl
{
    NOT_USED = 0,
    NEVER,
    SUPPORTED,
    PREFERED,
    REQUIRED
} TLevel;


/**
* Save options to cmd_params
*/
void save_options_to_cmdparams()
{
    gint tmp;
    set_parameter_text ((options.entry_charset), &cmd_params.charset);
    set_parameter_text ((options.entry_codepage), &cmd_params.codepage);
    set_parameter_text ((options.entry_dmode), &cmd_params.dmode);
    set_parameter_text ((options.entry_fmode), &cmd_params.fmode);
    set_parameter_text ((options.entry_gid), &cmd_params.gid);
    set_parameter_text ((options.entry_retrycount), &cmd_params.retry_count);
    set_parameter_text ((options.entry_timeout), &cmd_params.time_out);
    set_parameter_text ((options.entry_uid), &cmd_params.uid);
    set_parameter_text ((options.entry_volume), &cmd_params.volume);
    set_parameter_text ((options.entry_extraoptions), &cmd_params.extraoptions);

    /* Checkboxes */
    set_parameter_bool ((options.check_b), &cmd_params.b);
    set_parameter_bool ((options.check_C), &cmd_params.C);
    set_parameter_bool ((options.check_m), &cmd_params.m);
    set_parameter_bool ((options.check_s), &cmd_params.s);

    tmp = gtk_combo_box_get_active (GTK_COMBO_BOX (options.combo_signature));
    /* ComboBox */
    switch(tmp)
    {
    case NEVER:
        g_free(cmd_params.level);
        cmd_params.level = g_strdup("0");
        break;
    case SUPPORTED:
        g_free(cmd_params.level);
        cmd_params.level = g_strdup("1");
        break;
    case PREFERED:
        g_free(cmd_params.level);
        cmd_params.level = g_strdup("2");
        break;
    case REQUIRED:
        g_free(cmd_params.level);
        cmd_params.level = g_strdup("3");
        break;
    default:
        if(cmd_params.level != NULL)
        {
            g_free(cmd_params.level);
            cmd_params.level = NULL;
        }
        break;
    }
}



/**
* Prefill set options to dialog
*/
static void
set_options_from_cmdparams ()
{

    if(cmd_params.charset != NULL)
        gtk_entry_set_text(GTK_ENTRY(options.entry_charset),  g_shell_unquote(cmd_params.charset, NULL));
    else
        gtk_entry_set_text(GTK_ENTRY(options.entry_charset), "");

    if(cmd_params.codepage != NULL)
        gtk_entry_set_text(GTK_ENTRY(options.entry_codepage),  g_shell_unquote(cmd_params.codepage, NULL));
    else
        gtk_entry_set_text(GTK_ENTRY(options.entry_codepage), "");

    if(cmd_params.dmode != NULL)
        gtk_entry_set_text(GTK_ENTRY(options.entry_dmode),  g_shell_unquote(cmd_params.dmode, NULL));
    else
        gtk_entry_set_text(GTK_ENTRY(options.entry_dmode), "");

    if(cmd_params.fmode != NULL)
        gtk_entry_set_text(GTK_ENTRY(options.entry_fmode),  g_shell_unquote(cmd_params.fmode, NULL));
    else
        gtk_entry_set_text(GTK_ENTRY(options.entry_fmode), "");

    if(cmd_params.gid != NULL)
        gtk_entry_set_text(GTK_ENTRY(options.entry_gid),  g_shell_unquote(cmd_params.gid, NULL));
    else
        gtk_entry_set_text(GTK_ENTRY(options.entry_gid), "");

    if(cmd_params.retry_count != NULL)
        gtk_entry_set_text(GTK_ENTRY(options.entry_retrycount),  g_shell_unquote(cmd_params.retry_count, NULL));
    else
        gtk_entry_set_text(GTK_ENTRY(options.entry_retrycount), "");

    if(cmd_params.time_out != NULL)
        gtk_entry_set_text(GTK_ENTRY(options.entry_timeout),  g_shell_unquote(cmd_params.time_out, NULL));
    else
        gtk_entry_set_text(GTK_ENTRY(options.entry_timeout), "");

    if(cmd_params.uid != NULL)
        gtk_entry_set_text(GTK_ENTRY(options.entry_uid),  g_shell_unquote(cmd_params.uid, NULL));
    else
        gtk_entry_set_text(GTK_ENTRY(options.entry_uid), "");

    if(cmd_params.volume != NULL)
        gtk_entry_set_text(GTK_ENTRY(options.entry_volume),  g_shell_unquote(cmd_params.volume, NULL));
    else
        gtk_entry_set_text(GTK_ENTRY(options.entry_volume), "");

    if(cmd_params.extraoptions != NULL)
        gtk_entry_set_text(GTK_ENTRY(options.entry_extraoptions),  g_shell_unquote(cmd_params.extraoptions, NULL));
    else
        gtk_entry_set_text(GTK_ENTRY(options.entry_extraoptions), "");


    /* Checkboxes */
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (options.check_b), cmd_params.b);
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (options.check_C), cmd_params.C);
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (options.check_m), cmd_params.m);
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (options.check_s), cmd_params.s);

    /* ComboBox */
    if(cmd_params.level != NULL)
    {
        if(cmd_params.level[0] == '0')
            gtk_combo_box_set_active (GTK_COMBO_BOX (options.combo_signature), 1);
        else if(cmd_params.level[0] == '1')
            gtk_combo_box_set_active (GTK_COMBO_BOX (options.combo_signature), 2);
        else if(cmd_params.level[0] == '2')
            gtk_combo_box_set_active (GTK_COMBO_BOX (options.combo_signature), 3);
        else if(cmd_params.level[0] == '3')
            gtk_combo_box_set_active (GTK_COMBO_BOX (options.combo_signature), 4);
    }
    else
        gtk_combo_box_set_active (GTK_COMBO_BOX (options.combo_signature), 0);
}



/**
* Function opens option dialog box
*/
void show_options_dialog ()
{
    GtkWidget *label, *content_area, *halign, *hsep, *table;
    gint result, row;
    row  = 0;

    /* Create the widgets */
    options.dialog = gtk_dialog_new_with_buttons (_("Options"),
                     GTK_WINDOW (gui.win),
                     GTK_DIALOG_DESTROY_WITH_PARENT,
                     GTK_STOCK_OK, GTK_RESPONSE_OK,
                     GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                     NULL);

    content_area = gtk_dialog_get_content_area (GTK_DIALOG (options.dialog));

    table = gtk_table_new (21, 2, FALSE);
    gtk_container_add (GTK_CONTAINER (content_area), table);

    label = gtk_label_new (NULL);
    gtk_label_set_markup (GTK_LABEL (label), _("<span weight=\"bold\"><big>Set ncpmount options:</big></span>"));
    gtk_table_attach(GTK_TABLE(table), label, 0, 2, row, row+1,  GTK_FILL , GTK_FILL | GTK_EXPAND, 6, 0);
    row++;

    /* ----- */
    hsep = gtk_hseparator_new();
    gtk_table_attach (GTK_TABLE(table), hsep, 0, 2, row, row+1,  GTK_FILL | GTK_EXPAND, GTK_FILL | GTK_EXPAND, 6, 6);
    row++;

    halign = gtk_alignment_new(0, 0, 0, 1);
    label = gtk_label_new (_("Codepage [-p]:"));
    gtk_container_add(GTK_CONTAINER(halign), label);
    gtk_table_attach(GTK_TABLE(table), halign, 0, 1, row, row+1,  GTK_FILL , GTK_FILL | GTK_EXPAND, 6, 0);

    options.entry_codepage = gtk_entry_new ();
    gtk_table_attach_defaults (GTK_TABLE (table), options.entry_codepage, 1, 2, row, row+1);
    row++;

    halign = gtk_alignment_new(0, 0, 0, 1);
    label = gtk_label_new (_("Charset [-y]:"));
    gtk_container_add(GTK_CONTAINER(halign), label);
    gtk_table_attach(GTK_TABLE(table), halign, 0, 1, row, row+1,  GTK_FILL, GTK_FILL | GTK_EXPAND, 6, 0);

    options.entry_charset = gtk_entry_new ();
    gtk_table_attach_defaults (GTK_TABLE (table),  options.entry_charset, 1, 2, row, row+1);
    row++;

    /* ----- */
    hsep = gtk_hseparator_new();
    gtk_table_attach (GTK_TABLE(table), hsep, 0, 2,row, row+1,  GTK_FILL | GTK_EXPAND, GTK_FILL | GTK_EXPAND, 6, 6);
    row++;

    halign = gtk_alignment_new(0, 0, 0, 1);
    label = gtk_label_new (_("Signature level [-i]:"));
    gtk_container_add(GTK_CONTAINER(halign), label);
    gtk_table_attach(GTK_TABLE(table), halign, 0, 1, row, row+1,  GTK_FILL, GTK_FILL | GTK_EXPAND, 6, 0);

    options.combo_signature = gtk_combo_box_text_new();
    gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(options.combo_signature), _("Do not use signature level"));
    gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(options.combo_signature), _("Level 0 - never"));
    gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(options.combo_signature), _("Level 1 - supported"));
    gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(options.combo_signature), _("Level 2 - preferred"));
    gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(options.combo_signature), _("Level 3 - required"));

    gtk_combo_box_set_active(GTK_COMBO_BOX(options.combo_signature), 0);
    gtk_table_attach_defaults (GTK_TABLE (table),  options.combo_signature, 1, 2, row, row+1);
    row++;

    /* ----- */
    hsep = gtk_hseparator_new();
    gtk_table_attach (GTK_TABLE(table), hsep, 0, 2, row, row+1,  GTK_FILL | GTK_EXPAND, GTK_FILL | GTK_EXPAND, 6, 6);
    row++;

    halign = gtk_alignment_new(0, 0, 0, 1);
    label = gtk_label_new (_("Volume to mount [-V]:"));
    gtk_container_add(GTK_CONTAINER(halign), label);
    gtk_table_attach(GTK_TABLE(table), halign, 0, 1, row, row+1,  GTK_FILL , GTK_FILL | GTK_EXPAND, 6, 0);

    options.entry_volume = gtk_entry_new ();
    gtk_table_attach_defaults (GTK_TABLE (table), options.entry_volume, 1, 2, row, row+1);
    row++;

    /* ----- */
    hsep = gtk_hseparator_new();
    gtk_table_attach (GTK_TABLE(table), hsep, 0, 2, row, row+1,  GTK_FILL | GTK_EXPAND, GTK_FILL | GTK_EXPAND, 6, 6);
    row++;

    halign = gtk_alignment_new(0, 0, 0, 1);
    label = gtk_label_new (_("Mounted files uid [-u]:"));
    gtk_container_add(GTK_CONTAINER(halign), label);
    gtk_table_attach(GTK_TABLE(table), halign, 0, 1, row, row+1,  GTK_FILL , GTK_FILL | GTK_EXPAND, 6, 0);

    options.entry_uid = gtk_entry_new ();
    gtk_table_attach_defaults (GTK_TABLE (table), options.entry_uid , 1, 2, row, row+1);
    row++;

    halign = gtk_alignment_new(0, 0, 0, 1);
    label = gtk_label_new (_("Mounted files gid [-g]:"));
    gtk_container_add(GTK_CONTAINER(halign), label);
    gtk_table_attach(GTK_TABLE(table), halign, 0, 1, row, row+1,  GTK_FILL , GTK_FILL | GTK_EXPAND, 6, 0);

    options.entry_gid = gtk_entry_new ();
    gtk_table_attach_defaults (GTK_TABLE (table), options.entry_gid , 1, 2, row, row+1);
    row++;

    halign = gtk_alignment_new(0, 0, 0, 1);
    label = gtk_label_new (_("Files permission [-f]:"));
    gtk_container_add(GTK_CONTAINER(halign), label);
    gtk_table_attach(GTK_TABLE(table), halign, 0, 1, row, row+1,  GTK_FILL , GTK_FILL | GTK_EXPAND, 6, 0);

    options.entry_fmode = gtk_entry_new ();
    gtk_table_attach_defaults (GTK_TABLE (table), options.entry_fmode , 1, 2, row, row+1);
    row++;

    halign = gtk_alignment_new(0, 0, 0, 1);
    label = gtk_label_new (_("Dirs permission [-d]:"));
    gtk_container_add(GTK_CONTAINER(halign), label);
    gtk_table_attach(GTK_TABLE(table), halign, 0, 1, row, row+1,  GTK_FILL , GTK_FILL | GTK_EXPAND, 6, 0);

    options.entry_dmode = gtk_entry_new ();
    gtk_table_attach_defaults (GTK_TABLE (table), options.entry_dmode , 1, 2, row, row+1);
    row++;
    /* ----- */
    hsep = gtk_hseparator_new();
    gtk_table_attach (GTK_TABLE(table), hsep, 0, 2, row, row+1,  GTK_FILL | GTK_EXPAND, GTK_FILL | GTK_EXPAND, 6, 6);
    row++;

    halign = gtk_alignment_new(0, 0, 0, 1);
    label = gtk_label_new (_("Timeout [-t]:"));
    gtk_container_add(GTK_CONTAINER(halign), label);
    gtk_table_attach(GTK_TABLE(table), halign, 0, 1, row, row+1,  GTK_FILL , GTK_FILL | GTK_EXPAND, 6, 0);

    options.entry_timeout = gtk_entry_new ();
    gtk_table_attach_defaults (GTK_TABLE (table), options.entry_timeout , 1, 2, row, row+1);
    row++;

    halign = gtk_alignment_new(0, 0, 0, 1);
    label = gtk_label_new (_("Retry count [-r]:"));
    gtk_container_add(GTK_CONTAINER(halign), label);
    gtk_table_attach(GTK_TABLE(table), halign, 0, 1, row, row+1,  GTK_FILL , GTK_FILL | GTK_EXPAND, 6, 0);

    options.entry_retrycount = gtk_entry_new ();
    gtk_table_attach_defaults (GTK_TABLE (table), options.entry_retrycount , 1, 2, row, row+1);
    row++;

    /* ----- */
    hsep = gtk_hseparator_new();
    gtk_table_attach (GTK_TABLE(table), hsep, 0, 2, row, row+1,  GTK_FILL | GTK_EXPAND, GTK_FILL | GTK_EXPAND, 6, 6);
    row++;

    options.check_C = gtk_check_button_new_with_label (_("Don't convert password to uppercase [-C]"));
    gtk_table_attach_defaults (GTK_TABLE (table), options.check_C, 0, 2, row, row+1);
    row++;

    options.check_m = gtk_check_button_new_with_label (_("Allow multiple logins to server [-m]"));
    gtk_table_attach_defaults (GTK_TABLE (table), options.check_m, 0, 2, row, row+1);
    row++;

    options.check_s = gtk_check_button_new_with_label (_("Enable renaming/deletion of read-only files [-s]"));
    gtk_table_attach_defaults (GTK_TABLE (table), options.check_s, 0, 2, row, row+1);
    row++;

    options.check_b = gtk_check_button_new_with_label (_("Force bindery login to NDS servers [-b]"));
    gtk_table_attach_defaults (GTK_TABLE (table), options.check_b, 0, 2, row, row+1);
    row++;

    /* ----- */
    hsep = gtk_hseparator_new();
    gtk_table_attach (GTK_TABLE(table), hsep, 0, 2, row, row+1,  GTK_FILL | GTK_EXPAND, GTK_FILL | GTK_EXPAND, 6, 6);
    row++;

    halign = gtk_alignment_new(0, 0, 0, 1);
    label = gtk_label_new (_("Extra options:"));
    gtk_container_add(GTK_CONTAINER(halign), label);
    gtk_table_attach(GTK_TABLE(table), halign, 0, 1, row, row+1,  GTK_FILL , GTK_FILL | GTK_EXPAND, 6, 0);

    options.entry_extraoptions = gtk_entry_new ();
    gtk_table_attach_defaults (GTK_TABLE (table), options.entry_extraoptions , 1, 2, row, row+1);
    row++;

    /* ----- */
    hsep = gtk_hseparator_new();
    gtk_table_attach (GTK_TABLE(table), hsep, 0, 2, row, row+1,  GTK_FILL | GTK_EXPAND, GTK_FILL | GTK_EXPAND, 6, 6);
    row++;

    /* Prefill set options */
    set_options_from_cmdparams();

    gtk_widget_show_all (table);
    result = gtk_dialog_run (GTK_DIALOG(options.dialog));
    if(result == GTK_RESPONSE_OK)
    {
        save_options_to_cmdparams();
    }

    gtk_widget_destroy (options.dialog);
}



/**
* Show info, error or warning message
*/
void
show_message (GtkMessageType type, const gchar *format, gchar *msgtxt, const gchar *format_sec, gchar *msgtxt_sec)
{
    GtkWidget *dialog;

    dialog = gtk_message_dialog_new ( GTK_WINDOW (gui.win),
                                      GTK_DIALOG_DESTROY_WITH_PARENT,
                                      type,
                                      GTK_BUTTONS_OK,
                                      format,
                                      msgtxt);

    gtk_window_set_title (GTK_WINDOW (dialog), GUI_TITLE );

    gtk_message_dialog_format_secondary_text ( GTK_MESSAGE_DIALOG (dialog),
            format_sec,
            msgtxt_sec);
    gtk_dialog_run ( GTK_DIALOG  (dialog));
    gtk_widget_destroy (dialog);
}



/**
* Function shows ncpmount command with set options
*/
void show_ncpmount_command ()
{
    GtkWidget *dialog, *label, *content_area, *halign, *hsep, *entry_command, *vbox;

    gint strlen;

    gchar *command = get_ncpmount_command(FALSE,&strlen);

    /* Create the widgets */
    dialog = gtk_dialog_new_with_buttons (_("Show ncpmount command"),
                                          GTK_WINDOW (gui.win),
                                          GTK_DIALOG_DESTROY_WITH_PARENT,
                                          GTK_STOCK_CLOSE, GTK_RESPONSE_CLOSE,
                                          NULL);

    content_area = gtk_dialog_get_content_area (GTK_DIALOG (dialog));

    vbox = gtk_vbox_new(FALSE,6);
    gtk_container_add (GTK_CONTAINER (content_area), vbox);

    gtk_container_set_border_width (GTK_CONTAINER (vbox), 10);

    halign = gtk_alignment_new(0, 0, 0, 1);
    label = gtk_label_new (_("ncpmount command with selected options:"));
    gtk_container_add(GTK_CONTAINER(halign), label);
    gtk_box_pack_start (GTK_BOX (vbox),  halign,  FALSE, TRUE, 0);


    /* ----- */
    hsep = gtk_hseparator_new();
    gtk_box_pack_start (GTK_BOX (vbox), hsep,  FALSE, TRUE, 0);


    entry_command = gtk_entry_new ();
    gtk_box_pack_start (GTK_BOX (vbox), entry_command,  FALSE, TRUE, 0);
    gtk_editable_set_editable (GTK_EDITABLE(entry_command), FALSE);
    gtk_entry_set_text (GTK_ENTRY(entry_command), command);
    gtk_entry_set_width_chars (GTK_ENTRY(entry_command), strlen);

    /* ----- */
    hsep = gtk_hseparator_new();
    gtk_box_pack_start (GTK_BOX (vbox), hsep,  FALSE, TRUE, 0);

    gtk_widget_show_all(dialog);

    gtk_dialog_run ( GTK_DIALOG  (dialog));
    gtk_widget_destroy (dialog);

}


/**
* Function shows about dialog
*/
void show_about ()
{
    GtkWidget *dialog;
    GtkAboutDialog *about;
    GdkPixbuf *xpm;

    const gchar    *auth[] = { "Jan Lipovský <janlipovsky@gmail.com>",
                               NULL
                             };

   /* const gchar    *trans = "Jan Lipovský";*/


   const gchar    *license = "This program is licensed under the terms of the GNU General Public License version 3\r\n" \
                             "Available online under:\r\n\r\n" \
                             "http://www.gnu.org/licenses/gpl-3.0.html";

    dialog = gtk_about_dialog_new();
    gtk_window_set_transient_for( GTK_WINDOW( dialog ),
                                  GTK_WINDOW( gui.win ) );
    about = GTK_ABOUT_DIALOG( dialog );

    gtk_about_dialog_set_program_name( about, "gncpmount" );
    gtk_about_dialog_set_version( about, PROG_VERSION );
    gtk_about_dialog_set_copyright( about, "Copyright 2010 - 2011 © Jan Lipovský" );
    gtk_about_dialog_set_website( about, "http://gncpmount.lipoja.net" );
    gtk_about_dialog_set_website_label ( about, "gncpmount.lipoja.net" );
    gtk_about_dialog_set_authors( about, auth);
    gtk_about_dialog_set_comments( about, "graphical user interface (GUI) for ncpmount" );
    gtk_about_dialog_set_license (about, license);
    gtk_about_dialog_set_wrap_license (about, TRUE);
    gtk_about_dialog_set_license_type (about, GTK_LICENSE_GPL_3_0);
    gtk_about_dialog_set_logo_icon_name(about, "gncpmount");
/*
    gtk_about_dialog_set_translator_credits( about, trans );
*/

    xpm = gdk_pixbuf_new_from_xpm_data(about_xpm);
    gtk_about_dialog_set_logo( about, xpm );


    gtk_widget_show_all(dialog);
    gtk_dialog_run(GTK_DIALOG( dialog ));
    gtk_widget_destroy (dialog);
}
